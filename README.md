# ddf-wiktionnaire

Repo servant de stockage de la dernière version utilisable du wiktionnaire

PREFIX ddfd: <http://data.dictionnairedesfrancophones.org/>

|Date version du wiktionnaire |       Date version DDF    |       URI graphe nommé DDF       |
| --------------------------- |-------------------------- |----------------------------------|
| ?                           | 6 février 2020 00:00:00   |ddfd:dict/wikt/graph/1580943600000
| ?                           | 10 Février 2020 23:00:00  |ddfd:dict/wikt/graph/1581375600000|

